package com.starsky.dao;
import java.util.Properties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.github.pagehelper.PageHelper;

/**
 * 分页处理类
 *
 * @author Administrator
 *
 */
@Configuration
public class MybatisConfigure {

	@Bean
	public PageHelper pageHelper() {
		PageHelper page = new PageHelper();
		Properties p = new Properties();
		p.setProperty("offsetAsPageNum", "true");
		p.setProperty("rowBoundsWithCount", "true");
		p.setProperty("reasonable", "true");

		page.setProperties(p);
		return page;
	}
}
