package com.starsky.dao;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.starsky.entity.User;

public interface UserDaoMapper {

	@Select("select id,username as username,password from t_user where id=#{id}")
	public User getUserById(Integer id);


	@Select("select id,username as username,password from t_user where username=#{name}")
	public List<User> getUserByPage(String name);
//	自增长id,使用options配置返回id值
//	@Options(useGeneratedKeys=true,keyProperty="id",keyColumn="id")
	@Insert("insert into t_user(id,username,password) values(#{id},#{username},#{password})")
	public void saveUser(User user);
}
