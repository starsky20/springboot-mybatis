package com.starsky.dao;

import java.util.List;
import com.starsky.entity.User;

public interface UserMapper {

	List<User> queryList();

	void save(User user);

	void batchDelete(Integer[] ids);

	void update(User user);

}
