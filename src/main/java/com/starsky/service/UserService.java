package com.starsky.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.starsky.dao.UserDaoMapper;
import com.starsky.entity.User;

/**
 * 通过注解方式查询数据库
 *
 * @desc:
 * @author wangsh
 * @date 2018-3-4 下午12:04:56
 */
@Service
public class UserService {

	@Autowired
	private UserDaoMapper userDaoMapper;

	public User getUserById(Integer id) {
		return userDaoMapper.getUserById(id);
	}

	public List<User> getUserByPage(String name) {
		return userDaoMapper.getUserByPage(name);
	}

	public void saveUser(User user) {
		userDaoMapper.saveUser(user);
	}
}
