package com.starsky.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.starsky.dao.UserMapper;
import com.starsky.entity.User;

/**
 * 通过mybatis xml配置文件读取数据
 *
 * @desc:
 * @author wangsh
 * @date 2018-3-4 下午12:05:15
 */
@Service
public class UserService2 {

	@Autowired
	private UserMapper userMapper;

	public List<User> queryList() {
		List<User> queryList = userMapper.queryList();
		return queryList;
	}

	public void batchDelete(Integer[] ids) {
		userMapper.batchDelete(ids);
	}

	// REQUIRED:表示必须又事物管理，如果没有事物，就创建一个事物
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = Exception.class)
	public void update(User user) {
		userMapper.update(user);
	}

	// REQUIRED:表示必须又事物管理，如果没有事物，就创建一个事物
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = Exception.class)
	public void save(User user) {
		userMapper.save(user);
	}
}
