package com.starsky.util;

import java.util.UUID;

public class UUIDUtil {
	public static void main(String[] args) {
		System.out.println(getUUID());
	}

	public static String getUUID() {
		String str = UUID.randomUUID().toString().replace("-", "");
		return str;
	}
}
