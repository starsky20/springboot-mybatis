package com.starsky.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.starsky.entity.User;
import com.starsky.service.UserService;
import com.starsky.service.UserService2;
import com.starsky.util.UUIDUtil;

@Controller
public class UserController {

	/**
	 * 通过注解方式查询数据库
	 */
	@Autowired
	private UserService userService;

	@RequestMapping("/getUserById")
	@ResponseBody
	public User getUserById(Integer id) {
		User User = userService.getUserById(id);
		return User;
	}

	@RequestMapping("/getUserByPage")
	@ResponseBody
	public List<User> getUserByPage(String name) {
		// 设置分页
		PageHelper.startPage(1, 2);
		List<User> users = userService.getUserByPage(name);
		return users;
	}

	@RequestMapping("/save")
	@ResponseBody
	public User save() {
		User user = new User();
		user.setId(UUIDUtil.getUUID());
		user.setUsername("lisi");
		user.setPassword("lisi");
		userService.saveUser(user);
		return user;
	}

	/**
	 * 通过mybatis xml配置文件读取数据
	 */
	@Autowired
	private UserService2 userService2;

	@RequestMapping("/queryList")
	@ResponseBody
	public List<User> queryList() {
		List<User> queryList = userService2.queryList();
		return queryList;
	}

	@RequestMapping("/save2")
	@ResponseBody
	public User save2() {
		User user = new User();
		user.setId(UUIDUtil.getUUID());
		user.setUsername("lisi");
		user.setPassword("lisi");
		userService2.save(user);
		return user;
	}
}
